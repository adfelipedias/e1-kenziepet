from django.db import models
from django.db.models.deletion import CASCADE


class Characteristics(models.Model):
    name = models.CharField(max_length=255)


class Groups(models.Model):
    name = models.CharField(max_length=255)
    scientific_name = models.CharField(max_length=255)


class Animals(models.Model):
    name = models.CharField(max_length=255)
    age = models.FloatField()
    weight = models.FloatField()
    sex = models.CharField(max_length=100)

    group = models.ForeignKey(
        'Groups', on_delete=CASCADE, related_name='animals'
    )
    characteristics = models.ManyToManyField(
        'Characteristics', related_name='animals'
    )


