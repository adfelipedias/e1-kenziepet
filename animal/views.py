from django.core.exceptions import ObjectDoesNotExist
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from .serializers import AnimalSerializer
from .models import Animals, Groups, Characteristics


class AnimalView(APIView):
    def post(self, request):
        data = request.data

        serializer = AnimalSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

        group = data.pop('group')
        characteristics = data.pop('characteristics')

        current_group, created = Groups.objects.get_or_create(
            name=group['name'],
            scientific_name=group['scientific_name']
        )

        animal = Animals.objects.create(
            name=data['name'],
            age=data['age'],
            weight=data['weight'],
            sex=data['sex'],
            group=current_group,
        )

        for characteristic in characteristics:
            current_characteristics, created = Characteristics\
                .objects\
                .get_or_create(name=characteristic['name'])

            animal.characteristics.add(current_characteristics)

        serialized = AnimalSerializer(animal)
        return Response(serialized.data, status=status.HTTP_201_CREATED)

    def get(self, request):
        animals = Animals.objects.all()
        serialized = AnimalSerializer(animals, many=True)
        return Response(serialized.data, status=status.HTTP_200_OK)


class AnimalByIdView(APIView):
    def get(self, request, id=''):
        try:
            animal = Animals.objects.get(id=id)
            serialized = AnimalSerializer(animal)
            return Response(serialized.data, status=status.HTTP_200_OK)
        except ObjectDoesNotExist:
            return Response(
                {"Error": "Animal does not exists!"},
                status=status.HTTP_404_NOT_FOUND
            )

    def delete(self, request, id=''):
        try:
            animal = Animals.objects.get(id=id)
            animal.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except ObjectDoesNotExist:
            return Response(
                {"Error": "Animal does not exists!"},
                status=status.HTTP_404_NOT_FOUND
            )
