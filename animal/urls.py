from django.urls import path
from .views import AnimalByIdView, AnimalView

urlpatterns = [
    path('animals/', AnimalView.as_view()),
    path('animals/<str:id>/', AnimalByIdView.as_view()),
]
