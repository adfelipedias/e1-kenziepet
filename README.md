# KenziePet

## Descrição

</br>
É uma aplicação back-end para ajudar donos de PetShop a armazenar dados de animais
</br>

## Tecnologias:

- Liguagem:
 - Python 3

- FrameWork:
 - Django
 - REST Framework

- Banco de dados:
 - SQLite 3

## Diagrama ER
</br>
![diagrama_er](./DER.png)
</br>


## Endpoints Animals

### POST `api/animals/`

> Rota responsável por cadastrar animais

- Corpo da requisição:

```json
{
    "name": "Bidu",
    "age": 1,
    "weight": 30,
    "sex": "macho",
    "group": {
      "name": "cao",
      "scientific_name": "canis familiaris"
    },
    "characteristics": [
      {
        "name": "peludo"
      },
      {
        "name": "medio porte"
      }
    ]
}
```

- Corpo da resposta:
- RESPONSE STATUS -> HTTP 201
```json
{
    "id": 1,
    "name": "Bidu",
    "age": 1.0,
    "weight": 30.0,
    "sex": "macho",
    "group": {
      "id": 1,
      "name": "cao",
      "scientific_name": "canis familiaris"
    },
    "characteristics": [
      {
        "id": 1,
        "name": "peludo"
      },
      {
        "id": 2,
        "name": "medio porte"
      }
    ]
  }

```

### GET `api/animals/`

> Rota responsável por fazer a leitura de todos os animais cadastrados.

- Corpo da resposta:

- RESPONSE STATUS -> HTTP 200
```json
{
    [
    {
      "id": 1,
      "name": "Bidu",
      "age": 1,
      "weight": 30,
      "sex": "macho",
      "group": {
        "id": 1,
        "name": "cao",
        "scientific_name": "canis familiaris"
      },
      "characteristics": [
        {
          "id": 1,
          "name": "peludo"
        },
        {
          "id": 2,
          "name": "medio porte"
        }
      ]
    },
    {
      "id": 2,
      "name": "Hanna",
      "age": 1,
      "weight": 20,
      "sex": "femea",
      "group": {
        "id": 2,
        "name": "gato",
        "scientific_name": "felis catus"
      },
      "characteristics": [
        {
          "id": 1,
          "name": "peludo"
        },
        {
          "id": 3,
          "name": "felino"
        }
      ]
    }
  ]
}
```

### GET `api/animals/<int:animal_id>/`

> Rota responsável por fazer a leitura de um animais específico cadastrados, caso ele não exista é retornado um HTTP 404

- RESPONSE STATUS -> HTTP 200
```json
{
    "id": 1,
    "name": "Bidu",
    "age": 1,
    "weight": 30,
    "sex": "macho",
    "group": {
      "id": 1,
      "name": "cao",
      "scientific_name": "canis familiaris"
    },
    "characteristics": [
      {
        "id": 1,
        "name": "peludo"
      },
      {
        "id": 2,
        "name": "medio porte"
      }
    ]
  }
```

### DELETE `api/animals/<int:animal_id>/`

> Rota responsável por deletar um animal especificado.

- RESPONSE STATUS -> HTTP 204 (no content)
```json
```
